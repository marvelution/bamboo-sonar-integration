/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.tasks.rest.resources;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.5.5
 */
@XmlType(name="profile")
@XmlAccessorType(XmlAccessType.FIELD)
public class Profile {

	private String language;
	private String name;
	private boolean defaultProfile;

	/**
	 * Getter for language
	 *
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}
	
	/**
	 * Setter for language
	 *
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	
	/**
	 * Getter for name
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Setter for name
	 *
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Getter for defaultProfile
	 *
	 * @return the defaultProfile
	 */
	public boolean isDefaultProfile() {
		return defaultProfile;
	}
	
	/**
	 * Setter for defaultProfile
	 *
	 * @param defaultProfile the defaultProfile to set
	 */
	public void setDefaultProfile(boolean defaultProfile) {
		this.defaultProfile = defaultProfile;
	}

	/**
	 * Get a {@link Profile} from a given {@link org.sonar.wsclient.services.Profile}
	 * 
	 * @param sonarProfile the {@link org.sonar.wsclient.services.Profile} to transform
	 * @return the {@link Profile}
	 */
	public static Profile get(org.sonar.wsclient.services.Profile sonarProfile) {
		Profile profile = new Profile();
		profile.setName(sonarProfile.getName());
		profile.setLanguage(sonarProfile.getLanguage());
		profile.setDefaultProfile(sonarProfile.isDefaultProfile());
		return profile;
	}

}
