/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.tasks.utils;

import net.java.ao.EntityManager;

import org.apache.log4j.Logger;

/**
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.5.3
 */
public class ActiveObjectsUtils {

	private static final Logger logger = Logger.getLogger(ActiveObjectsUtils.class);

	/**
	 * Checker for the auto-commit status, if auto-commit is off then commit() is called on the {@link EntityManager}
	 */
	public static void checkAutoCommit(EntityManager entityManager) {
		try {
			// I found that during testing the commit is not always executed leaving the check to be only in the cache
			// So calling the commit method manually in case auto-commit is off
			if (!entityManager.getProvider().getConnection().getAutoCommit()) {
				logger.debug("Auto-commit is off, calling commit() manually...");
				entityManager.getProvider().getConnection().commit();
			}
		} catch (Exception e) {
			logger.error("Failed to check auto-commit status and/or manually commiting the changes", e);
		}
	}

}
