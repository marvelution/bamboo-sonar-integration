/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

(function($, BAMBOO) {
	BAMBOO.SONAR = {};

    /**
     * Sonar Task form
     * 
     * @param {Object} opts - options with which to set up the form
     * @constructor
     */
	function TaskForm(opts) {
		var defaults = {
				selectors: {
					sonar: null,
		        	sonarUrl: null,
		            username: null,
		            password: null,
		            profile: null,
		            language: null
				}
			};
		// Options
		this.options = $.extend(true, defaults, opts);
		// Fields
		this.$sonar = $(this.options.selectors.sonar);
		this.$sonarUrl = $(this.options.selectors.sonarUrl);
		this.$username = $(this.options.selectors.username);
		this.$password = $(this.options.selectors.password);
		this.$profile = $(this.options.selectors.profile);
		this.$language = $(this.options.selectors.language);
		this.jqXHR = null;
	}

	TaskForm.prototype = {
		init : function() {
			// Attach handlers
			this.$profile.siblings('input[type="button"]').click(_.bind(this.loadProfilesClick, this));
			this.$profile.change(_.bind(this.setLanguage, this));
			// Load profiles if needed
			if (this.$sonar.val() || this.$sonarUrl.val()) {
				this.loadProfiles(this.$profile);
			}
		},
		loadProfilesClick: function() {
			this.clearFieldErrors(this.$profile);
			this.$profile.prop('disabled', true);
			this.loadProfiles(this.$profile);
		},
		setLanguage: function() {
			var language = this.$profile.children(':selected').data('language');
			if (language) {
				this.$language.val(language);
			} else {
				this.$language.val("");
			}
		},
		loadProfiles: function() {
			var $button = this.$profile.siblings('input[type="button"]'),
				$selector = this.$profile,
				$loadingOptgroup = $('<optgroup />').attr('label', AJS.I18n.getText('sonar.loading.profiles')).appendTo($selector),
				currentProfile = $selector.val(),
				currentLanguage = this.$language.val(),
				generateOption = this.generateOption,
				update = _.bind(function(json, textStatus, jqXHR) {
					var data = json['data'];
					if (data) {
						var $list = $();
						$list = $list.add($('<option/>', {
							text : AJS.I18n.getText('sonar.implicit.profile'),
							val : ''
						}));
						$.each(data, function() {
							$list = $list.add(generateOption(this));
						});
						$selector.empty().append($list).prop('disabled', false);
						if (currentProfile != '') {
							$selector.children('option[label="' + currentProfile + ' (' + currentLanguage + ')"]').attr('selected', 'selected');
						} else {
							$selector.val('');
						}
						$selector.change();
					} else {
						showError(jqXHR);
					}
				}, this),
				restore = _.bind(function () {
					$loadingOptgroup.remove();
					$button.prop('disabled', false);
				}, this),
				showError = _.bind(function (jqXHR) {
					var response, errors = [];
                    if (jqXHR.statusText != 'abort') {
                        try {
                            response = $.parseJSON(jqXHR.responseText);
                            errors = response['errors'];
                        } catch (e) {};
                        if (!errors.length) {
                            errors.push(AJS.I18n.getText('sonar.rest.ajaxError') + '[' + jqXHR.status + ' ' + jqXHR.statusText + ']');
                        }
                        this.addFieldErrors($selector, errors);
                    }
				}, this);
			$button.prop('disabled', true);
			return this.getProfiles().done(update).fail(showError).always(restore);
		},
		generateOption: function(profile) {
			return $('<option/>', {
				label : profile['name'] + " (" + profile['language'] + ")",
				text : profile['name'] + " (" + profile['language'] + ")",
				val : profile['name'],
				data : profile
			});
		},
		getProfiles: function() {
			var url = AJS.contextPath() + '/rest/sonar/latest/profiles',
				$container = this.$profile.closest('.field-group'),
				loadingClass = 'loading',
				data = {};
			if (this.$sonar.val() != 0) {
				data.sonarId = this.$sonar.val();
			} else {
				data.baseUrl = this.$sonarUrl.val();
				data.username = this.$username.val();
				data.password = this.$password.val();
			}
			if (this.jqXHR) {
				this.jqXHR.abort();
			}
			$container.addClass(loadingClass);
			return this.jqXHR = $.ajax({
				type: 'POST',
				url: url,
				data: data,
				dataType: 'json'
			}).always(function() {
				$container.removeClass(loadingClass);
			});
		},
		addFieldErrors : function($field, errors) {
			var $fieldArea = this.getFieldArea($field),
				$description = $fieldArea.find('.description'),
				$errors = BAMBOO.buildFieldError(errors);
			$errors.hide();
			if ($description.length) {
				$description.before($errors);
			} else if ($fieldArea.length) {
				$fieldArea.append($errors);
			} else {
				$field.after($errors);
			}
			$errors.slideDown();
		},
		clearFieldErrors : function($field) {
			this.getFieldArea($field).find('.error').slideUp(function() {
				$(this).remove();
			});
		},
		getFieldArea: function($field) {
			return $field.closest('.sonar-profile');
		}
	}

	BAMBOO.SONAR.TaskForm = TaskForm;

}(jQuery, window.BAMBOO = (window.BAMBOO || {})));
