/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.web.actions.admin.bulk;

import static com.marvelution.bamboo.plugins.sonar.common.configuration.SonarConfigConstants.*;

import java.util.Map;

import org.apache.log4j.Logger;
import org.osgi.util.tracker.ServiceTracker;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.ww2.actions.admin.bulk.BulkAction;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.marvelution.bamboo.plugins.sonar.common.configuration.SonarTaskConfigurator;
import com.marvelution.bamboo.plugins.sonar.common.servers.SimpleSonarServerManager;
import com.marvelution.bamboo.plugins.sonar.web.proxy.OsgiServiceProxy;

/**
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class UpdateSonarServerBulkAction extends UpdateSonarHostBulkAction {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(UpdateSonarServerBulkAction.class);

	private final OsgiContainerManager osgiContainerManager;
	private ServiceTracker serviceTracker;
	private SimpleSonarServerManager manager;

	public UpdateSonarServerBulkAction(OsgiContainerManager osgiContainerManager) {
		this.osgiContainerManager = osgiContainerManager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getKey() {
		return "bulk.action.sonar.server.update";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getTitle() {
		return getText("bulkAction.sonarServer.title");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getChangedItem() {
		return getText("bulkAction.sonarServer.changeItem");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WebWorkAction getViewAction() {
		return new BulkAction.WebWorkActionImpl(ACTION_NAMESPACE, "viewSonarServer");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WebWorkAction getViewUpdatedAction() {
		return new BulkAction.WebWorkActionImpl(ACTION_NAMESPACE, "viewUpdatedSonarServer");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WebWorkAction getEditSnippetAction() {
		return new BulkAction.WebWorkActionImpl(ACTION_NAMESPACE, "editSonarServer");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validateTaskConfiguration(SonarTaskConfigurator taskConfigurator, ActionParametersMap params,
					ErrorCollection errorCollection) {
		taskConfigurator.validateSonarServer(params, errorCollection);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, String> getTaskConfigurationMap(Map<String, String[]> params) {
		Map<String, String> config = super.getTaskConfigurationMap(params);
		config.put(CFG_SONAR_ID, String.valueOf(getNewServerId(params)));
		return config;
	}

	/**
	 * Get the new Sonar Server Id
	 * 
	 * @param params the submitted parameters
	 * @return the new Server Id, may be <code>0</code>
	 */
	public int getNewServerId(Map<String, String[]> params) {
		if (params.get(CFG_SONAR_ID) != null) {
			LOGGER.debug("New " + CFG_SONAR_ID + " is set");
			return Integer.parseInt(params.get(CFG_SONAR_ID)[0]);
		}
		LOGGER.debug("New " + CFG_SONAR_ID + " is not set");
		return 0;
	}

	/**
	 * Getter of the new Server Name
	 * 
	 * @param params the submitted parameters
	 * @return the name of the new sonar server
	 */
	public String getNewServerName(Map<String, String[]> params) {
		if (params.get(CFG_SONAR_ID) != null) {
			LOGGER.debug("New " + CFG_SONAR_ID + " is set");
			return getSonarServerManager().getServerName(getNewServerId(params));
		}
		LOGGER.debug("New " + CFG_SONAR_ID + " is not set");
		return "";
	}

	/**
	 * Getter of the Id Name {@link Map} of all the Sonar Servers
	 * 
	 * @return the {@link Map} of servers
	 */
	public Map<Integer, String> getSonarServers() {
		return getSonarServerManager().getServersAsMap();
	}

	/**
	 * Getter of the {@link SimpleSonarServerManager} from the OSGI container
	 * 
	 * @return the {@link SimpleSonarServerManager}
	 */
	public SimpleSonarServerManager getSonarServerManager() {
		if (serviceTracker == null) {
			serviceTracker = osgiContainerManager.getServiceTracker(SimpleSonarServerManager.class.getName());
		}
		if (manager != null) {
			return manager;
		} else {
			return manager = OsgiServiceProxy.getServiceProxy(serviceTracker, SimpleSonarServerManager.class);
		}
	}

}
